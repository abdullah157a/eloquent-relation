<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 mt-5">
                <form action="{{ url('Coursestore') }}" method="post" >
                    @csrf
                    <input type="hidden" value="{{$studentid}}" name="sid" >
                    <div class="form-group">
                      <label for="">CourseName:</label>
                    <input type="text" name="coursename" id="" class="form-control" placeholder="Enter your Course Name" value="{{old('coursename')}}" >
                    </div>
                    
                    <div class="form-group">
                    <label for="">Department:</label>
                    <input type="text" name="department" id="" class="form-control" placeholder="Enter your Course Name" value="{{old('department')}}" >
                    </div>
        
                    <br>
                    <input type="submit" value="Submit Form" class="btn btn-info btn-lg">
                    <button class="btn btn-success btn-lg  float-right" ><a href="{{ url('Studentdisplay') }}" class="text-white"> Back To User</a></button>
                </form>
            </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
        
      </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>