<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('studentsAdd', [StudentController::class, 'create']);
Route::post('Studentstore', [StudentController::class, 'store']);
Route::get('Studentdisplay', [StudentController::class, 'index']);

Route::get('CourseForm{id}', [CourseController::class, 'create']);
Route::post('Coursestore', [CourseController::class, 'store']);
