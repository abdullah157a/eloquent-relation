<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return view('studentdisplay', compact('students'));
    }
    public function create()
    {
        return view('addstudent');
    }

    public function store(Request $request)
    {
        $image = $request->file('image');
        $my_image = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('upload'), $my_image);
        Student::create([
            'name' => $request->name,
            'image' => $my_image,
        ]);
        return Redirect::to('Studentdisplay');
    }
}
