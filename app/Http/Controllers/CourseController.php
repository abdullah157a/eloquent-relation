<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{


    public function create($id)
    {


        return view('addcourse')->with('studentid', $id);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // die;
        $students = new Course;
        $students->coursename = $request->input('coursename');
        $students->departement = $request->input('department');
        $students->user_id = $request->input('sid');
        $students->save();
        return Redirect::to('CourseForm' . $students->user_id);
    }
}
